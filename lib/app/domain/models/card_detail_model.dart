class CardDetailModel {
  final int id;
  final String title;
  final String subtitle;
  final String imagePath;

  CardDetailModel({
    required this.id,
    required this.title,
    required this.subtitle,
    required this.imagePath,
  });
}
