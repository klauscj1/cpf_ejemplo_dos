import 'package:ejemplo_dos/app/domain/models/card_detail_model.dart';

class CardModel {
  final int id;
  final String title;
  final String description;
  final double percent;
  final String imagePath;
  final List<CardDetailModel> items;

  CardModel({
    required this.id,
    required this.title,
    required this.description,
    required this.percent,
    required this.imagePath,
    required this.items,
  });
}

List<CardModel> cards = [
  CardModel(
    id: 1,
    title: 'WEEK 1',
    description: 'Rock',
    percent: 20.0,
    imagePath: 'assets/images/card1.jpg',
    items: [
      CardDetailModel(
          id: 1,
          title: 'Rock uno',
          subtitle: "subtitulo rock uno",
          imagePath: 'assets/images/card1.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Rock dos',
          subtitle: "subtitulo rock dos",
          imagePath: 'assets/images/card1.jpg'),
    ],
  ),
  CardModel(
    id: 2,
    title: 'WEEK 2',
    description: 'Pop',
    percent: 30.0,
    imagePath: 'assets/images/card2.jpg',
    items: [
      CardDetailModel(
          id: 1,
          title: 'Pop uno',
          subtitle: "subtitulo Pop uno",
          imagePath: 'assets/images/card1.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Rock dos',
          subtitle: "subtitulo Pop dos",
          imagePath: 'assets/images/card2.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Rock tres',
          subtitle: "subtitulo Pop tres",
          imagePath: 'assets/images/card2.jpg'),
    ],
  ),
  CardModel(
    id: 3,
    title: 'WEEK 3',
    description: 'Pop rock',
    percent: 80.0,
    imagePath: 'assets/images/card3.jpg',
    items: [
      CardDetailModel(
          id: 1,
          title: 'Pop rock uno',
          subtitle: "subtitulo Pop rock uno",
          imagePath: 'assets/images/card3.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Pop Rock dos',
          subtitle: "subtitulo Pop rock dos",
          imagePath: 'assets/images/card3.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Pop Rock tres',
          subtitle: "subtitulo Pop rock tres",
          imagePath: 'assets/images/card3.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Pop Rock cuatro',
          subtitle: "subtitulo Pop rock cuatro",
          imagePath: 'assets/images/card3.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Pop Rock cinco',
          subtitle: "subtitulo Pop rock cinco",
          imagePath: 'assets/images/card3.jpg'),
    ],
  ),
  CardModel(
    id: 4,
    title: 'WEEK 4',
    description: 'Classic',
    percent: 10.0,
    imagePath: 'assets/images/card4.jpg',
    items: [
      CardDetailModel(
          id: 1,
          title: 'Classic uno',
          subtitle: "subtitulo Classic uno",
          imagePath: 'assets/images/card4.jpg'),
      CardDetailModel(
          id: 2,
          title: 'Classic dos',
          subtitle: "subtitulo Classic dos",
          imagePath: 'assets/images/card4.jpg'),
    ],
  ),
  CardModel(
    id: 5,
    title: 'WEEK 5',
    description: 'Others',
    percent: 50.0,
    imagePath: 'assets/images/card5.jpg',
    items: [
      CardDetailModel(
          id: 1,
          title: 'Others uno',
          subtitle: "subtitulo Others uno",
          imagePath: 'assets/images/card5.jpg'),
    ],
  ),
];
