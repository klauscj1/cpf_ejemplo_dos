import 'package:ejemplo_dos/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            Icon(
              Icons.menu,
              color: AppColors.color2,
            ),
            CircleAvatar(
              radius: 21,
              backgroundImage: AssetImage('assets/images/usuario.jpg'),
            )
          ],
        ),
      ),
    );
  }
}
