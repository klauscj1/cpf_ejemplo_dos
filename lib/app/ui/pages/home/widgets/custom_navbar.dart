import 'package:ejemplo_dos/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomButtonNavbar extends StatelessWidget {
  const CustomButtonNavbar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        height: 60,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(.14),
                blurRadius: 20,
                offset: const Offset(3, -3),
                spreadRadius: 1)
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const [
            Icon(
              Icons.home,
              color: AppColors.color1,
              size: 25,
            ),
            Icon(
              Icons.compass_calibration,
              color: AppColors.color2,
              size: 25,
            ),
            Icon(
              Icons.videocam,
              color: AppColors.color2,
              size: 25,
            ),
            Icon(
              Icons.person,
              color: AppColors.color2,
              size: 25,
            )
          ],
        ),
      ),
    );
  }
}
