import 'package:ejemplo_dos/app/domain/models/card_model.dart';
import 'package:ejemplo_dos/app/ui/pages/home/widgets/card_item.dart';
import 'package:flutter/material.dart';

class CardItemList extends StatelessWidget {
  const CardItemList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      scrollDirection: Axis.vertical,
      physics: const BouncingScrollPhysics(),
      itemCount: cards.length,
      itemBuilder: (_, index) {
        return CardItem(
          card: cards[index],
        );
      },
    );
  }
}
