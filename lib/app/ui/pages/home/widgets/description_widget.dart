import 'package:ejemplo_dos/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class DescriptionWidget extends StatelessWidget {
  const DescriptionWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Training program",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Text(
            "12 weeks",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: AppColors.color2,
                  fontWeight: FontWeight.normal,
                ),
          ),
        ],
      ),
    );
  }
}
