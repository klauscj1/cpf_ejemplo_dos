import 'package:ejemplo_dos/app/ui/global_widgets/title_text.dart';
import 'package:flutter/material.dart';

import 'widgets/card_item_list.dart';
import 'widgets/custom_appbar.dart';
import 'widgets/custom_navbar.dart';
import 'widgets/description_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  CustomAppBar(),
                  Padding(
                    padding: EdgeInsets.only(top: 30, bottom: 5),
                    child: Hero(child: TitleText(), tag: 'logo'),
                  ),
                  DescriptionWidget(),
                  Expanded(
                    child: CardItemList(),
                  ),
                ],
              ),
            ),
          ),
          const CustomButtonNavbar()
        ],
      ),
    );
  }
}
