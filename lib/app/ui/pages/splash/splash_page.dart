import 'package:ejemplo_dos/app/ui/global_widgets/title_text.dart';
import 'package:ejemplo_dos/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Hero(
          child: TitleText(),
          tag: 'logo',
        ),
      ),
    );
  }

  void _init() async {
    await Future.delayed(const Duration(milliseconds: 1300));
    Navigator.pushReplacementNamed(context, Routes.home);
  }
}
