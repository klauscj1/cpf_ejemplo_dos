import 'package:ejemplo_dos/app/domain/models/card_model.dart';
import 'package:flutter/material.dart';

import 'custom_back_button.dart';
import 'player_controls.dart';
import 'player_widget.dart';

class ImagePlayer extends StatelessWidget {
  const ImagePlayer({
    Key? key,
    required this.card,
  }) : super(key: key);

  final CardModel card;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * .33,
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(40),
              bottomRight: Radius.circular(40),
            ),
            child: SizedBox(
              height: MediaQuery.of(context).size.height * .33,
              width: double.infinity,
              child: Hero(
                tag: "img-${card.id}",
                child: Image.asset(
                  card.imagePath,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 10),
                child: CustomBackButton(),
              ),
              const PlayerControls(),
              Player(card: card)
            ],
          )
        ],
      ),
    );
  }
}
