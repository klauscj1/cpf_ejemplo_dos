import 'package:ejemplo_dos/app/domain/models/card_model.dart';
import 'package:ejemplo_dos/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class Player extends StatelessWidget {
  const Player({
    Key? key,
    required this.card,
  }) : super(key: key);

  final CardModel card;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          const Text(
            "00:00",
            style: TextStyle(color: AppColors.color3),
          ),
          const SizedBox(
            width: 5,
          ),
          Expanded(
            child: SizedBox(
              height: 5,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.color2,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  Container(
                    width: card.percent,
                    decoration: BoxDecoration(
                      color: AppColors.color1,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 5,
          ),
          const Text(
            "03:21",
            style: TextStyle(color: AppColors.color3),
          ),
          const Icon(
            Icons.fullscreen,
            color: AppColors.color3,
          )
        ],
      ),
    );
  }
}
