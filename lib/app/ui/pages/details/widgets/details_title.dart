import 'package:ejemplo_dos/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class DetailsTitle extends StatelessWidget {
  const DetailsTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "STRETCH",
          style: Theme.of(context)
              .textTheme
              .headline6!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        Text(
          "SEE ALL",
          style: Theme.of(context)
              .textTheme
              .caption!
              .copyWith(color: AppColors.color2),
        ),
      ],
    );
  }
}
