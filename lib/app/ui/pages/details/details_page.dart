import 'package:ejemplo_dos/app/domain/models/card_model.dart';
import 'package:flutter/material.dart';

import 'widgets/card_detail_widget.dart';
import 'widgets/details_title.dart';
import 'widgets/image_player.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({Key? key, required this.card}) : super(key: key);
  final CardModel card;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ImagePlayer(card: card),
          const Padding(
            padding: EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 25,
            ),
            child: DetailsTitle(),
          ),
          Expanded(
            child: ListView.separated(
              physics: const BouncingScrollPhysics(),
              padding: const EdgeInsets.symmetric(
                vertical: 20,
                horizontal: 25,
              ),
              itemCount: card.items.length,
              itemBuilder: (_, int index) {
                return CardDetailWidget(
                  detail: card.items[index],
                );
              },
              separatorBuilder: (_, int index) {
                return const SizedBox(
                  height: 17,
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
