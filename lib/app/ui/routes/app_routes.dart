import 'package:ejemplo_dos/app/ui/pages/home/home_page.dart';
import 'package:ejemplo_dos/app/ui/pages/splash/splash_page.dart';
import 'package:ejemplo_dos/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

Map<String, Widget Function(BuildContext)> routes = {
  Routes.splash: (_) => const SplashPage(),
  Routes.home: (_) => const HomePage(),
};
