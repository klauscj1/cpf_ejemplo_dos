import 'package:flutter/material.dart';

class AppColors {
  static const Color color1 = Color(0XFFFFC300);
  static const Color color2 = Color(0XFFA0A0A0);
  static const Color color3 = Color(0XFFFFFFFF);
}
