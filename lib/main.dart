import 'package:ejemplo_dos/app/ui/routes/app_routes.dart';
import 'package:ejemplo_dos/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ejemplo dos',
      initialRoute: Routes.splash,
      routes: routes,
    );
  }
}
